'use strict';

const Student = (props) => {
	console.log(props);
	const { id, nama, jurusan, pasFoto } = props;

	return (
		<figure key={id}>
			<img src={`img/${pasFoto}`} alt={nama} width="300" height="300" />
			<figcaption>
				{nama} ({jurusan})
			</figcaption>
		</figure>
	);
};

const RegistrationStudent = () => {
	const students = [
		{
			id: '01',
			nama: 'Eka',
			jurusan: 'Teknik Informatika',
			pasFoto: 'koala.png',
		},
		{
			id: '02',
			nama: 'Lisa',
			jurusan: 'Sistem Informasi',
			pasFoto: 'jellyfish.png',
		},
		{
			id: '03',
			nama: 'Rudi',
			jurusan: 'Teknik Elektro',
			pasFoto: 'elephant.png',
		},
	];

	return (
		<React.Fragment>
			<h1>Registration Student - University Ilkom</h1>
			<hr />
			<section style={{ display: 'flex', textAlign: 'center', justifyContent: 'center' }}>
				{students.map((student) => (
					<Student key={student.id} nama={student.nama} jurusan={student.jurusan} pasFoto={student.pasFoto} />
				))}
			</section>
		</React.Fragment>
	);
};

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(<RegistrationStudent />);
