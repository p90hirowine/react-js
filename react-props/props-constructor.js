'use strict';

class Learn extends React.Component {
	constructor(props) {
		super(props);
		const { material } = props;
		this.title = material.toLocaleUpperCase();
	}

	render() {
		return <h1>Learn {this.title}</h1>;
	}
}

const MyComponent = <Learn material="React" />;
const root = document.getElementById('root');
ReactDOM.createRoot(root).render(MyComponent);
