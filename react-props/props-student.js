'use strict';

class Student extends React.Component {
	constructor(props) {
		super(props);
		console.log(props);
	}

	render() {
		const { nama, jurusan, pasFoto } = this.props;
		return (
			<section style={{ display: 'flex', textAlign: 'center', justifyContent: 'center' }}>
				<figure>
					<img src={`img/${pasFoto}`} alt={nama} width="300" height="300" />
					<figcaption>
						{nama} ({jurusan})
					</figcaption>
				</figure>
			</section>
		);
	}
}

class RegistrationStudent extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const mahasiswa = {
			nama: 'Lisa',
			jurusan: 'Sistem Informasi',
			pasFoto: 'jellyfish.png',
		};

		return <Student nama={mahasiswa.nama} jurusan={mahasiswa.jurusan} pasFoto={mahasiswa.pasFoto} />;
	}
}

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(<RegistrationStudent />);
