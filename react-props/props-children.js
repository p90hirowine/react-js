'use strict';

class Learn extends React.Component {
	constructor(props) {
		super(props);
		console.log(props);
	}

	render() {
		const { id, children } = this.props;
		return <h1 id={id}>Learn {children}</h1>;
	}
}

const MyComponent = <Learn id="01">React</Learn>;
const root = document.getElementById('root');
ReactDOM.createRoot(root).render(MyComponent);
