'use strict';

class Learn extends React.Component {
	constructor(props) {
		super(props);
		console.log(props);
	}

	render() {
		const { material } = this.props;
		return <h1>Learn {material}</h1>;
	}
}

const MyComponent = (
	<div>
		<Learn material="React" />
		<Learn material="JavaScript" />
		<Learn material="Nodejs" />
	</div>
);

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(MyComponent);
