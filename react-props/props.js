'use strict';

class LearnProps extends React.Component {
	constructor(props) {
		super(props);
		console.log(props);
	}

	render() {
		const { material } = this.props;
		return <h1>Learn {material}</h1>;
	}
}

const MyComponent = <LearnProps material="react" id="01" className="judul" />;
const root = document.getElementById('root');
ReactDOM.createRoot(root).render(MyComponent);
