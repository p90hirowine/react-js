'use strict';

const Learn = (props) => {
	const { material } = props;
	return <h1>Learn {material}</h1>;
};

const MyComponent = (
	<div>
		<Learn material="React" />
		<Learn material="JavaScript" />
		<Learn material="Nodejs" />
	</div>
);

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(MyComponent);
