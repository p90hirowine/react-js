'use strict';

class TitleReact extends React.Component {
	render() {
		return <h2>Learning React-js</h2>;
	}
}

class TitleJavaScript extends React.Component {
	render() {
		return <h2>Learning JavaScript</h2>;
	}
}

class MyComponent extends React.Component {
	render() {
		return (
			<div>
				<TitleReact />
				<hr />
				<TitleJavaScript />
			</div>
		);
	}
}

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(<MyComponent />);
