'use strict';

class TitleReact extends React.Component {
	render() {
		return <h2>Learning React-js</h2>;
	}
}

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(<TitleReact />);
