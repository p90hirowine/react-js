'use strict';

class MyComponent extends React.Component {
	constructor() {
		super();
		this.titleReact = 'Learn React';
	}

	titleReactLowerCase() {
		return this.titleReact.toLocaleLowerCase();
	}

	render() {
		return (
			<React.Fragment>
				<h1>{this.titleReact}</h1>
				<hr />
				<p>Stt... serious to {this.titleReactLowerCase()}</p>
			</React.Fragment>
		);
	}
}

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(<MyComponent />);
