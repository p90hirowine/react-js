'use strict';

class TitleReact extends React.Component {
	render() {
		return <h2>Learning React-js</h2>;
	}
}

class TitleJavaScript extends React.Component {
	render() {
		return <h2>Learning JavaScript</h2>;
	}
}

const container = (
	<div>
		<TitleReact />
		<hr />
		<TitleJavaScript />
	</div>
);

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(container);
