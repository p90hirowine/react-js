'use strict';

function TitleReact() {
	return <h2>Learning React</h2>;
}

function TitleJavascript() {
	return <h2>Learning JavaScript</h2>;
}

function MyComponent() {
	return (
		<div>
			<TitleReact />
			<hr />
			<TitleJavascript />
		</div>
	);
}

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(<MyComponent />);
