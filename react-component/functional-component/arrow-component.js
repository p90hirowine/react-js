'use strict';

const TitleReact = () => <h2>Learning React</h2>;
const TitleJavascript = () => <h2>Learning JavaScript</h2>;

const MyComponent = () => {
	return (
		<div>
			<TitleReact />
			<hr />
			<TitleJavascript />
		</div>
	);
};

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(<MyComponent />);
