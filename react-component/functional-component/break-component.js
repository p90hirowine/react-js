'use strict';

const students = [
	{
		id: '01',
		nama: 'Eka',
		jurusan: 'Teknik Informatika',
		pasFoto: 'koala.png',
	},
	{
		id: '02',
		nama: 'Lisa',
		jurusan: 'Sistem Informasi',
		pasFoto: 'jellyfish.png',
	},
	{
		id: '03',
		nama: 'Rudi',
		jurusan: 'Teknik Elektro',
		pasFoto: 'elephant.png',
	},
];

const Student = () => {
	return (
		<section style={{ display: 'flex', textAlign: 'center', justifyContent: 'center' }}>
			{students.map((student) => (
				<figure key={student.id}>
					<img src={'img/' + student.pasFoto} alt={student.nama} width="300" height="300" />
					<figcaption>
						{student.nama} ({student.jurusan})
					</figcaption>
				</figure>
			))}
		</section>
	);
};

const RegistrationStudents = () => {
	return (
		<React.Fragment>
			<h1>Students Registration - Ilkom University</h1>
			<hr />
			<Student />
		</React.Fragment>
	);
};

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(<RegistrationStudents />);
