'use strict';

const myElement = React.createElement('ul', {}, [
	React.createElement('li', { key: 'a' }, React.createElement('a', { href: 'https://id.wikipedia.org/wiki/Espreso' }, 'Espreso')),
	React.createElement('li', { key: 'b' }, React.createElement('a', { href: 'https://id.wikipedia.org/wiki/Kapucino' }, 'Cappuccino')),
	React.createElement('li', { key: 'c' }, React.createElement('a', { href: 'https://id.wikipedia.org/wiki/Moka' }, 'Moccacino')),
]);

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(myElement);
