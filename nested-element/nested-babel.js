'use strict';

const myElement = (
	<ul>
		<li>
			<a href="https://id.wikipedia.org/wiki/Espreso">Espreso</a>
		</li>
		<li>
			<a href="https://id.wikipedia.org/wiki/Kapucino">Cappuccino</a>
		</li>
		<li>
			<a href="https://id.wikipedia.org/wiki/Moka">Moccacino</a>
		</li>
	</ul>
);

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(myElement);
