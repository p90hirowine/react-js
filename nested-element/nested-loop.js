'use strict';

const myArray = [
	['Espreso', 'https://id.wikipedia.org/wiki/Espreso'],
	['Cappuccino', 'https://id.wikipedia.org/wiki/Kapucino'],
	['Moccacino', 'https://id.wikipedia.org/wiki/Moka'],
];

const myElement = (
	<ul>
		{myArray.map((kopi) => (
			<li key={kopi[0]}>
				<a href={kopi[1]}>{kopi[0]}</a>
			</li>
		))}
	</ul>
);

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(myElement);
