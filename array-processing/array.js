'use strict';

const myArray = [
	{
		nama: 'Eka',
		umur: 19,
		jurusan: 'Teknik Informatika',
	},
	{
		nama: 'Lisa',
		umur: 18,
		jurusan: 'Sistem Informasi',
	},
	{
		nama: 'Rudi',
		umur: 19,
		jurusan: 'Teknik Elektro',
	},
];

const myElement = (
	<table>
		<thead>
			<tr>
				<th>Nama</th>
				<th>Umur</th>
				<th>Jurusan</th>
			</tr>
		</thead>
		<tbody>
			{myArray.map((mhs, i) => (
				<tr key={i}>
					<td>{mhs.nama}</td>
					<td>{mhs.umur}</td>
					<td>{mhs.jurusan}</td>
				</tr>
			))}
		</tbody>
	</table>
);

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(myElement);
