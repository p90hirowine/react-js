'use strict';

class MyApp extends React.Component {
	buttonClickHandler() {
		alert('Hello React !');
	}

	render() {
		return (
			<div>
				<h1>Learn React</h1>
				<button onClick={this.buttonClickHandler}>Click me</button>
			</div>
		);
	}
}

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(<MyApp />);
