'use strict';

class Button extends React.Component {
	buttonClickHandler = () => {
		alert(this.props.massage);
	};

	render() {
		return (
			<button onClick={this.buttonClickHandler} style={{ margin: '10px' }}>
				{this.props.children}
			</button>
		);
	}
}

class MyApp extends React.Component {
	render() {
		return (
			<div>
				<h1>Learn React</h1>
				<Button massage="Learn React">React</Button>
				<Button massage="Learn JavaScript">JavaScript</Button>
			</div>
		);
	}
}

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(<MyApp />);
