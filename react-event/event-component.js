'use strict';

class Button extends React.Component {
	buttonClickHandler() {
		alert('Hello React !');
	}

	render() {
		return <button onClick={this.buttonClickHandler}>Click me</button>;
	}
}

class MyApp extends React.Component {
	render() {
		return (
			<div>
				<h1>Learn React</h1>
				<Button />
			</div>
		);
	}
}

const root = document.getElementById('root');
ReactDOM.createRoot(root).render(<MyApp />);
