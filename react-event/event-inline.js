'use strict';

const myElement = <button onClick={() => alert('Hello World !')}>Click me</button>;
const root = document.getElementById('root');
ReactDOM.createRoot(root).render(myElement);
